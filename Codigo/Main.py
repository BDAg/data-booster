#importaçao de bibliotecas
import requests
from lxml import html
import datetime
import pyttsx3
import csv

#funçoes de formataçao
def a(val):
    a.retur = val

def f(s, arg):
    a(s[int(arg[1]) - 1 : int(arg[2]) + 1])

def m(s, arg):
    a(s.replace(arg[1], '', int(arg[2])))

def r(s, arg):
    a(s.replace(arg[1], arg[2], int(arg[3])))

def mf(s, arg):
    s = str(s.strip() + ' ' + arg[1].strip())
    a(s)

def mi(s, arg):
    s = str(arg[1].strip() + ' ' + s.strip())
    a(s)

def dict(s, arg):
    dic = {}
    for di in range(1, len(arg)):
        dic[arg[di][ : arg[di].find(':')]] = arg[di][arg[di].find(':') + 1 : ]
    a(dic[str(s.strip())])

#declaraçao de variaveis
docr = open('sites.txt', 'r', encoding='utf-8').read().splitlines()
falar = open('fala.txt', 'r', encoding='utf-8').read()
nomtxt = []
sittxt = []
camtxt = []
met = []
fortxt = {}
rel = datetime.datetime.now()
valsit = {'hora': rel.strftime('%H'), 'minuto': str(int(rel.strftime('%m'))), 'ano': rel.year, 'mes': rel.month, 'mesescrito': {1: 'Janeiro', 2: 'Fevereiro', 3: 'Março', 4: 'Abril', 5: 'Maio', 6: 'Junho', 7: 'Julho', 8: 'Agosto', 9: 'Setembro', 10: 'Outubro', 11: 'Novembro', 12: 'Desembro'}[rel.month], 'dia': rel.day, 'diadasemana': {0: 'Segunda-feira', 1: 'Terça-feira', 2: 'Quarta-feira', 3: 'Quinta-feira', 4: 'Sexta-feira', 5: 'Sabado', 6: 'Domingo'}[rel.weekday()]}
sitvrf = []

#leitura os arwuivos txt
for i in range (len(docr)):
    nomtxt.append(docr[i][:docr[i].find('=')].strip())
    sittxt.append(docr[i][docr[i].find('=') + 1:docr[i].find(',')].strip())
    if '|' in docr[i]:
        camtxt.append(docr[i][docr[i].find(',') + 1: docr[i].find('|')].strip())
        skp = docr[i].find('|') + 1
        fortxt[str(docr[i][:docr[i].find('=')].strip())] = []
        for vi in range(docr[i][docr[i].find('|') :].count(',') + 1):
            fortxt[str(docr[i][:docr[i].find('=')].strip())].append(docr[i][skp : docr[i].find(',', skp + 1) if docr[i][docr[i].find('|') :].count(',') >= 1 and vi != docr[i][docr[i].find('|') :].count(',') else len(docr[i])])
            skp = docr[i].find(',', skp + 1) + 1 if vi != docr[i][docr[i].find('|') :].count(',') else 0
    else:
        camtxt.append(docr[i][docr[i].find(',') + 1:].strip())
        fortxt[str(docr[i][:docr[i].find('=')].strip())] = 'null'
    met.append(['xpath'])
    if len(camtxt[i].split()) > 1:
        met[i] = camtxt[i].split()
        met[i].pop(len(camtxt[i].split()) - 1)
        camtxt[i] = camtxt[i].split()[len(camtxt[i].split()) - 1]

#coleta de dADOS
for i in range (len(sittxt)):
    pagina = requests.get(sittxt[i])
    pagina = html.fromstring(pagina.content)
    skip = 0
    for h in range (sittxt.count(sittxt[i])):
        if sittxt[i] in sitvrf:
            break
        else:
            try:
                valsit[nomtxt[sittxt.index(sittxt[i], skip)]] = pagina.xpath(camtxt[sittxt.index(sittxt[i], skip)])[0].text
                if fortxt[nomtxt[sittxt.index(sittxt[i], skip)]] != 'null':
                    for fm in fortxt[nomtxt[sittxt.index(sittxt[i], skip)]]:
                        v = valsit[nomtxt[sittxt.index(sittxt[i], skip)]]
                        prm = fm.strip().split()
                        exec(f'{prm[0]}(v, prm)')
                        valsit[nomtxt[sittxt.index(sittxt[i], skip)]] = a.retur
                skip = sittxt.index(sittxt[i], skip) + 1
            except:
                print('erro coletando informações do caminho {}, no site {}, favor verificar os dados informados'.format(camtxt[sittxt.index(sittxt[i], skip)], sittxt[i]))
    sitvrf.append(sittxt[i])

#inicio variaveis padroes
nomtxt.append('ano')
nomtxt.append('mes')
nomtxt.append('dia')
nomtxt.append('diadasemana')
nomtxt.append('hora')
nomtxt.append('minuto')
nomtxt.append('mesescrito')

#substituiçao no texto
for i in range (len(nomtxt)):
    if str('(' + nomtxt[i] + ')') in falar:
        falar = falar.replace(('(' + nomtxt[i] + ')'), str(valsit[nomtxt[i]].strip()))

#salvar em csv
csvf = open('base.csv', 'a', newline='')
ecsv = csv.writer(csvf, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
ecsv.writerow(valsit.values())

#sistema de fala
print(falar)
engine = pyttsx3.init()
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[0].id)
engine.say(falar)
engine.runAndWait()