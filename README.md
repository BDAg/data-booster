<img src="https://i.imgur.com/ez6YBzs.jpg"/>

## Descrição 
O programa ***Data Booster*** foi desenvolvido na disciplina de Projeto Integrador de Programação, com a finalidade de **buscar** e **capturar na internet** dados pré-definidos pelo usuário e mostrar de maneira audiovisual, agilizando sua vida e evitando que você perca tempo com pesquisas repetitivas, além disso o projeto permite **formatar os dados capturados** da maneira que for conveniente e **formar uma frase para ser dita por uma IA** (utilizando da biblioteca **PYTTSX3**) com essas informações. Esses dados também são armazenados em um arquivo **CSV**.<br>
Para a apresentação do projeto foi criado um [pitch](https://youtu.be/9g22MGSBjEQ) detalhando o serviço oferecido pelo web crawler. O vídeo pode ser acessado em: [https://youtu.be/9g22MGSBjEQ](https://youtu.be/9g22MGSBjEQ).

## Ferramentas e Bibliotecas 
O banco de dados utilizado foi **CSV**. Na parte técnica, como sistemas e linguagens, foi  utilizado **Python** e suas bibliotecas: **LXML**, **PYTTSX3**, **Datetime** e **requests**, além da IDE **PyCharm** (para fazer o código) e o  **Git** (para disponibilizar o código).

### Linguagenss de programação
 - [Python](https://www.python.org/).

### Bibliotecas
 - [LXML](https://lxml.de);
 - [PYTTSX3](https://pyttsx3.readthedocs.io/en/latest/);
 - [Datetime](https://docs.python.org/3/library/datetime.html);
 - [requests](https://docs.python-requests.org/en/master/).

### Como utilizar? 
Abra o arquivo ***sites.txt*** (adicionado dentro da pasta “Código”) e adicione o **nome da variável a ser utilizada**, seguida pelo **link do site** onde se encontra a informação a ser capturada, e por fim o **XPATH** para essa informação, na seguinte sintaxe: 

`Nome da variável = link do site, XPATH`

Caso deseje, também é possível formatar o que foi coletado utilizando o caractere “` | `”, na seguinte sintaxe: 

`Nome da variável = link do site, XPATH | Formatação`

#### As formatações disponíveis são:
 - **f**: define o caractere inicial e final do dado coletado;

	`... | f **inicio** **final**`

 - **m**: remove um caractere especifico;

	`... | m **caracter_a_ser_removido**`

 - **r**: substitui um caractere especifico por outro;
 
	`... | r **caracter_a_ser_trocado** **caracter_para_substituir**`
    
 - **mf**: adiciona uma string no final;
 
    `... | mf **string_a_ser_adicionada**`
    
 - **mi**: adiciona uma string no começo;
 
    `... | mi **string_a_ser_adicionada**`
    
 - **dict**: cria um dicionário para a substituição de uma string por outra.
 
    `... | dict **item1=substituição1** **item2=substituição2** **item3=substituição3** ...`

### Desenvolvedores do Projeto:
 - Enzo Luiz Tsutsumi de Almeida José
 - Felipe Marins Cardoso
 - Lucas Alexandre Inácio Cardoso 
 - Luiz Antonio Anjos
 - Luiz Gustavo Fernandes Salvador 

### Mentor 
 - Prof. Dr. Luís Hilário Tobler Garcia